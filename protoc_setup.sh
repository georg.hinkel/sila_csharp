#! /bin/bash

# directory where the script is located
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ `uname` == "Linux" ]
then
  OS=linux
else
  OS=macosx
fi
if [ `uname -i` == "x86_64" ]
then
  echo Using proto compiler for 64 bit $OS system
  PLATFORM=${OS}_x64
else
  echo Using proto compiler for 32 bit $OS system
  PLATFORM=${OS}_x86
fi
PROTOC_PATH=$DIR/sila_tools/Grpc.Tools.1.7.1/tools/$PLATFORM

export PROTOC=$PROTOC_PATH/protoc
export CSHARP_PLUGIN=protoc-gen-grpc=$PROTOC_PATH/grpc_csharp_plugin

export FRAMEWORK_PATH="--proto_path ${DIR}/sila_base/protobuf"

chmod +x $PROTOC
chmod +x $PROTOC_PATH/grpc_csharp_plugin