namespace sila2.Utils
{
    using CommandLine;

    public class ServerCmdLineArgs
    {
        [Option('p', "port", Default = 50052, HelpText = "Port to connect server to.")]
        public int Port { get; set; }

        [Option('c', "config", Required = false, HelpText = "Path to file to store/load server configuration.")]
        public string ConfigFile { get; set; }
    }
}