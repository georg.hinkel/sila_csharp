﻿namespace DataTypeProviderServer
{
    using System;
    using System.Threading;
    using Common.Logging;
    using sila2;
    using sila2.Utils;
    using CommandLine;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class DataTypeProviderServer : SiLA2Server
    {
        private readonly DataTypeProviderImpl _dataTypeProviderImpl;
        private static ILog Log = LogManager.GetLogger<DataTypeProviderServer>();

        public DataTypeProviderServer(int portNumber, string configFile)
        : base(new ServerInformation(
                    "DataTypeProvider Server",
                    "Server that implements the DataTypeProvider feature which provides calls using all available SiLA Data Types for their parameters",
                    "www.equicon.de",
                    "1.0"),
                portNumber,
                configFile)
        {
            this.ReadFeature("features/DataTypeProvider.xml");
            _dataTypeProviderImpl = new DataTypeProviderImpl();
            this.GrpcServer.Services.Add(Sila2.Org.Silastandard.Examples.Datatypeprovider.V1.DataTypeProvider.BindService(_dataTypeProviderImpl));
        }

        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = LogManager.GetLogger<DataTypeProviderServer>();
            try
            {
                // Parse cmd line args
                ServerCmdLineArgs options = null;
                Parser.Default.ParseArguments<ServerCmdLineArgs>(args)
                   .WithParsed<ServerCmdLineArgs>(o => options = o);

                // create SiLA2 Server
                // start the gRPC Server
                DataTypeProviderServer server = new DataTypeProviderServer(options.Port, options.ConfigFile);
                server.StartServer();
                Log.Info("DataTypeProvider Server listening on port " + options.Port);

                Thread.Sleep(3000);
                Log.Info("\nPress enter to stop the server...");
                Console.ReadLine();

                server.ShutdownServer();
            }
            catch (Exception e)
            {
                Log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}
