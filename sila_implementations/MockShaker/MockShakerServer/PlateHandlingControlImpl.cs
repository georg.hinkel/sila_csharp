﻿namespace ShakerServer
{
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Logging;
    using Grpc.Core;
    using sila2;
    using SiLAFramework = Sila2.Org.Silastandard;
    using Sila2.De.Equicon.Handling.Platehandlingcontrol.V1;

    internal class PlateHandlingControlImpl : PlateHandlingControl.PlateHandlingControlBase
    {
        private static ILog Log = LogManager.GetLogger<PlateHandlingControlImpl>();
        private readonly ShakerSimulator _shakerSimulator;
        public ClampState CurrentClampState { get; private set; }

        public PlateHandlingControlImpl(ShakerSimulator shakerSimulator)
        {
            _shakerSimulator = shakerSimulator;
        }

        #region Overrides of PlateHandlingControlBase
        public override Task<OpenClamp_Responses> OpenClamp(OpenClamp_Parameters request, ServerCallContext context)
        {
            Log.Info($"\n\"{new StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            // check if currently shaking
            if (_shakerSimulator.IsShaking)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardExecutionError("DeviceCurrentlyShakingError",
                    "Clamp can not be opened because device is currently shaking",
                    "Wait for the device to finish shaking"));
            }

            // simulate clamp opening
            Thread.Sleep(1000);

            CurrentClampState = ClampState.Open;

            return Task.FromResult(new OpenClamp_Responses());
        }

        public override Task<CloseClamp_Responses> CloseClamp(CloseClamp_Parameters request, ServerCallContext context)
        {
            Log.Info($"\n\"{new StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            // check if currently shaking
            if (_shakerSimulator.IsShaking)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardExecutionError("DeviceCurrentlyShakingError",
                    "Clamp can not be closed because device is currently shaking",
                    "Wait for the device to finish shaking"));
            }

            // simulate clamp closing
            Thread.Sleep(1000);

            CurrentClampState = ClampState.Closed;
            return Task.FromResult(new CloseClamp_Responses());
        }

        public override Task<Get_CurrentClampState_Responses> Get_CurrentClampState(Get_CurrentClampState_Parameters request, ServerCallContext context)
        {
            Log.Info($"\n\"{new StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            return Task.FromResult(new Get_CurrentClampState_Responses { CurrentClampState = new SiLAFramework.Boolean { Value = CurrentClampState == ClampState.Open } });
        }

        #endregion
    }
}