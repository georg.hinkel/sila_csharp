﻿namespace SiLAClient
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Grpc.Core;
    using Common.Logging;
    using sila2;
    using sila2.Discovery;
    using SiLAFramework = Sila2.Org.Silastandard;
    using SiLAService = Sila2.Org.Silastandard.Core.Silaservice.V1;
    using GreetingProvider = Sila2.Org.Silastandard.Examples.Greetingprovider.V1;
    using TemperatureController = Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
    using sila2.Utils;

    /// <summary>
    /// Implements a SiLA 2 client that connects to the SiLA 2 server 'ExampleServer' and uses the functionality of its features.
    /// </summary>
    public class ExampleClient
    {
        #region Instances of the gRPC clients for the implemented features (generated by the proto compiler)

        public SiLAService.SiLAService.SiLAServiceClient SilaServiceClient { private get; set; }
        public GreetingProvider.GreetingProvider.GreetingProviderClient GreetingProviderClient { private get; set; }
        public TemperatureController.TemperatureController.TemperatureControllerClient TemperatureControllerClient { private get; set; }
        private static ILog Log = LogManager.GetLogger<ExampleClient>();

        #endregion

        #region SiLAService commands and properties

        public string GetServerName()
        {
            SiLAService.Get_ServerName_Responses response;
            try
            {
                response = SilaServiceClient.Get_ServerName(new SiLAService.Get_ServerName_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerName.Value;
        }

        public string GetServerDescription()
        {
            SiLAService.Get_ServerDescription_Responses response;
            try
            {
                response = SilaServiceClient.Get_ServerDescription(new SiLAService.Get_ServerDescription_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerDescription.Value;
        }

        public string GetServerVendorUrl()
        {
            SiLAService.Get_ServerVendorURL_Responses response;
            try
            {
                response = SilaServiceClient.Get_ServerVendorURL(new SiLAService.Get_ServerVendorURL_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
                return string.Empty;
            }

            return response.ServerVendorURL.URL.Value;
        }

        public string GetServerVersion()
        {
            SiLAService.Get_ServerVersion_Responses response;
            try
            {
                response = SilaServiceClient.Get_ServerVersion(new SiLAService.Get_ServerVersion_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
                return string.Empty;
            }

            return response.ServerVersion.Value;
        }

        public List<string> GetImplementedFeatures()
        {
            List<string> result = new List<string>();
            try
            {
                var response = SilaServiceClient.Get_ImplementedFeatures(new SiLAService.Get_ImplementedFeatures_Parameters());

                foreach (var featureQualifier in response.ImplementedFeatures)
                {
                    result.Add(featureQualifier.FeatureIdentifier.Value);
                }
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
            }

            return result;
        }

        public Feature GetFeatureDefinition(string featureIdentifier)
        {
            SiLAService.GetFeatureDefinition_Responses response;
            try
            {
                response = SilaServiceClient.GetFeatureDefinition(new SiLAService.GetFeatureDefinition_Parameters { QualifiedFeatureIdentifier = new SiLAService.DataType_FeatureIdentifier { FeatureIdentifier = new SiLAFramework.String { Value = featureIdentifier } } });
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
                return null;
            }


            try
            {
                return FeatureGenerator.ReadFeatureFromXml(response.FeatureDefinition.FeatureDefinition.Value);
            }
            catch (Exception e)
            {
                Log.Error("GetFeatureDefinition failed: returned object is not a valid Feature definition, Details: " + e.Message);
                return null;
            }
        }

        #endregion

        #region GreetingProvider commands and properties

        public string SayHello(string name)
        {
            GreetingProvider.SayHello_Responses response;
            try
            {
                response = GreetingProviderClient.SayHello(new GreetingProvider.SayHello_Parameters { Name = new SiLAFramework.String { Value = name } });
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
                return string.Empty;
            }

            return response.Greeting.Value;
        }

        public int GetStartYear()
        {
            GreetingProvider.Get_StartYear_Responses response;
            try
            {
                response = GreetingProviderClient.Get_StartYear(new GreetingProvider.Get_StartYear_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error((ErrorHandling.HandleException(e)));
                return -1;
            }

            return (int)response.StartYear.Value;
        }

        #endregion

        #region TemperatureController commands and properties

        public async Task ControlTemperature(double temperature, bool oneLineOutput = false)
        {
            try
            {
                // start command
                var cmdId = TemperatureControllerClient.ControlTemperature(new TemperatureController.ControlTemperature_Parameters { TargetTemperature = new SiLAFramework.Real { Value = temperature } }).CommandId;
                Console.WriteLine("ControlTemperature command started...");

                // wait for command excution to finish
                using (var call = TemperatureControllerClient.ControlTemperature_State(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        var message = string.Format("--> Command ControlTemperature    -status: {0}   -remaining time: {1,3:###}s    -progress: {2}", responseStream.Current.CommandStatus, responseStream.Current.EstimatedRemainingTime.Seconds, FormatProgress(responseStream.Current.ProgressInfo.Value));
                        Console.ForegroundColor = ConsoleColor.DarkMagenta;
                        if (oneLineOutput)
                        {
                            Console.Write("\r" + message);
                        }
                        else
                        {
                            Console.WriteLine(message);
                        }
                        if (responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                    Console.Write("\n");
                }

                // get result (empty response here, so this is not neccessarry - just for demonstration)
                TemperatureController.ControlTemperature_Responses response = TemperatureControllerClient.ControlTemperature_Result(cmdId);
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Calls the ControlTemperature command but does not use ControlTemperature_State to wait for the execution to be finished (calls ControlTemperature_Result immediately)
        /// </summary>
        /// <param name="temperature">The target temperature</param>
        public void ControlTemperatureNoWait(double temperature)
        {
            Console.WriteLine("Trying to get the result right after the ControlTemperature command has been started...");

            try
            {
                var cmdId = TemperatureControllerClient.ControlTemperature(new TemperatureController.ControlTemperature_Parameters { TargetTemperature = new SiLAFramework.Real { Value = temperature } }).CommandId;
                TemperatureController.ControlTemperature_Responses response = TemperatureControllerClient.ControlTemperature_Result(cmdId);
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subscribes to the CurrentTemperature property and listens endless.
        /// </summary>
        public async Task GetCurrentTemperature()
        {
            try
            {
                using (var responseStream = TemperatureControllerClient.Subscribe_CurrentTemperature(new TemperatureController.Subscribe_CurrentTemperature_Parameters()).ResponseStream)
                {
                    while (await responseStream.MoveNext())
                    {
                        WriteTemperatureString(responseStream.Current.CurrentTemperature.Value);
                    }
                }
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subsribes to the CurrentTemperature property for the given duration. The call is cancelled afterwards.
        /// </summary>
        public async Task GetCurrentTemperature(TimeSpan duration)
        {
            try
            {
                var cts = new CancellationTokenSource();
                using (var responseStream = TemperatureControllerClient.Subscribe_CurrentTemperature(new TemperatureController.Subscribe_CurrentTemperature_Parameters(), Metadata.Empty, null, cts.Token).ResponseStream)
                {
                    var startTime = DateTime.Now;
                    while (await responseStream.MoveNext() && DateTime.Now - startTime < duration)
                    {
                        WriteTemperatureString(responseStream.Current.CurrentTemperature.Value);
                    }

                    cts.Cancel();
                    responseStream.Dispose();
                }
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        /// <summary>
        /// Subsribes to the CurrentTemperature property and receives the given number of values before the stream will be cancelled.
        /// </summary>
        public async Task GetCurrentTemperature(int numberOfValues)
        {
            try
            {
                var cts = new CancellationTokenSource();
                using (var responseStream = TemperatureControllerClient.Subscribe_CurrentTemperature(new TemperatureController.Subscribe_CurrentTemperature_Parameters(), Metadata.Empty, null, cts.Token).ResponseStream)
                {
                    var valueReceived = 0;
                    while (await responseStream.MoveNext() && valueReceived++ < numberOfValues)
                    {
                        WriteTemperatureString(responseStream.Current.CurrentTemperature.Value);
                    }

                    cts.Cancel();
                    responseStream.Dispose();
                }
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        #endregion

        #region Helper methods

        private static void WriteTemperatureString(double temperature)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("--> Property CurrentTemperature: {0:0.0} °C   ({1:0.0} °F)", UnitConverter.Kelvin2DegreeCelsius(temperature), UnitConverter.Kelvin2DegreeFahrenheit(temperature));
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static string FormatProgress(double progress)
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0,3:###}%  [", Math.Round(progress * 100.0));
            for (int i = 0; i < 10; i++)
            {
                sb.Append((int)Math.Round(progress * 10.0) > i ? "#" : " ");
            }
            sb.Append("]");

            return sb.ToString();
        }

        #endregion

        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<ExampleClient>();
            try
            {
                Channel channel;
                // use SiLA Server Discovery to find the Example Server
                const string serviceType = "_sila._tcp";
                string namePattern = "Example Server";
                Console.WriteLine("\nServer Discovery:\nQuerying for service type \"{0}\" with name pattern \"{1}\"...", serviceType, namePattern);
                var foundServices = ServiceFinder.QueryAllServicesByName(serviceType, namePattern, 2000);
                if (foundServices.Count > 0)
                {
                    Console.WriteLine("{0} service{1} found:", foundServices.Count, foundServices.Count == 1 ? string.Empty : "s");
                    foreach (var service in foundServices)
                    {
                        Console.WriteLine(" - \"{0}\"(host: {1}) at {2}", service.ServiceName, service.HostName, service.Address);
                    }

                    // create channel
                    Console.WriteLine("\nConnecting to service \"{0}\" on {1} ...", foundServices[0].ServiceName, foundServices[0].Address);
                    channel = new Channel(foundServices[0].Address, ChannelCredentials.Insecure);
                }
                else
                {
                    // in case SiLA Service Discovery didn't work, use command line service port (default: 50052)
                    int port = args != null && args.Length > 0 ? int.Parse(args[0]) : 50052;
                    channel = new Channel("127.0.0.1:" + port, ChannelCredentials.Insecure);
                }

                // create client
                var client = new ExampleClient
                {
                    SilaServiceClient = new SiLAService.SiLAService.SiLAServiceClient(channel),
                    GreetingProviderClient = new GreetingProvider.GreetingProvider.GreetingProviderClient(channel),
                    TemperatureControllerClient = new TemperatureController.TemperatureController.TemperatureControllerClient(channel)
                };

                // read properties from SiLAService feature
                Console.WriteLine("Server display name: " + client.GetServerName());
                Console.WriteLine("Server description: " + client.GetServerDescription());
                Console.WriteLine("Server vendor URL: " + client.GetServerVendorUrl());
                Console.WriteLine("Server version: " + client.GetServerVersion());

                // feature discovery
                Console.WriteLine("\nFeature Discovery:");

                // get implemented features
                var list = client.GetImplementedFeatures();
                Console.WriteLine("{0} implemented Feature{1} found:", list.Count, list.Count == 1 ? string.Empty : "s");
                foreach (var f in list)
                {
                    Console.WriteLine(" - " + f);
                }

                // get feature definitions
                foreach (var featureQualifier in list)
                {
                    Console.WriteLine("\n-------------------------------------------------------------------------");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(" Feature Definition of " + featureQualifier);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("-------------------------------------------------------------------------");
                    var featureDefinition = client.GetFeatureDefinition(featureQualifier);
                    Console.WriteLine("namespace: " + featureDefinition.Namespace);
                    Console.WriteLine(featureDefinition.ToString());
                }

                Console.WriteLine("\n\n");

                // try getting a feature definition with invalid feature identifier
                Console.WriteLine("\nCalling \"GetFeatureDefinition(\"something\")\" ...");
                client.GetFeatureDefinition("something");

                // try temperature control without greeting :)
                Console.WriteLine("\nCalling \"ControlTemperature(300.0)\" ...");
                client.ControlTemperature(300).Wait();

                // command SayHello
                Console.WriteLine("\nCalling \"SayHello(\"SiLA user\")\" ...");
                Console.WriteLine(client.SayHello("SiLA user"));

                // property CurrentYear
                Console.WriteLine("\nCalling \"StartYear()\" ...");
                Console.WriteLine(client.GetStartYear());

                // check range validation
                Console.WriteLine("\nCalling \"ControlTemperature(373.15)\" ...");
                client.ControlTemperature(373.15).Wait();

                Console.WriteLine("\nCalling \"ControlTemperature(273.0)\" ...");
                client.ControlTemperature(273.0).Wait();

                // start temperature control and getting the result without waiting for the execution to be finished
                Console.WriteLine("\nCalling \"ControlTemperature(30 °C)\" and ControlTemperature_Result() right afterwards");
                client.ControlTemperatureNoWait(UnitConverter.DegreeCelsius2Kelvin(30));

                // start temperature control with valid conditions
                Console.WriteLine("\nCalling \"ControlTemperature(30 °C)\" ...");
                client.ControlTemperature(UnitConverter.DegreeCelsius2Kelvin(30), true).Wait();

                // subscribe to temperature property for 5 seconds
                Console.WriteLine("\nCalling \"Subscribe_CurrentTemperature()\" for 5 seconds ...");
                client.GetCurrentTemperature(new TimeSpan(0, 0, 5)).Wait();

                Thread.Sleep(1000);

                // subscribe to temperature property for receiving 3 values
                Console.WriteLine("\nCalling \"Subscribe_CurrentTemperature()\" for receiving 3 values ...");
                client.GetCurrentTemperature(3).Wait();

                Thread.Sleep(1000);

                // subscribe to temperature property
                Console.WriteLine("\nCalling \"Subscribe_CurrentTemperature()\" listening without canceling ...");
                client.GetCurrentTemperature();

                Thread.Sleep(3000);

                // start another temperature control with running property outputs
                Console.WriteLine("\nCalling \"ControlTemperature(45 °C)\" ...");
                client.ControlTemperature(UnitConverter.DegreeCelsius2Kelvin(45)).Wait();

                Thread.Sleep(10000);
                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                log.Error("Client Error: ", e);
                return;
            }

            Console.ReadKey();
        }
    }
}