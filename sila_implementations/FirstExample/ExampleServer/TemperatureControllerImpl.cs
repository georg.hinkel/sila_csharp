﻿namespace ExampleServer
{
    using System;
    using System.ComponentModel;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Logging;
    using Grpc.Core;
    using sila2;
    using SiLAFramework = Sila2.Org.Silastandard;
    using Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
    using sila2.Utils;

    /// <summary>
    /// Implements the functionality of the TemperatureControl feature.
    /// </summary>
    internal class TemperatureControllerImpl : TemperatureController.TemperatureControllerBase
    {
        #region Private memebers
        private static ILog Log = LogManager.GetLogger<TemperatureControllerImpl>();
        private const string ControlTemperatureCmdId = "ControlTemperature";
        private double _currentTemperature = UnitConverter.DegreeCelsius2Kelvin(20);
        private double _targetTemperature;
        private double _temperatureTolerance = 0.5;
        private int _executionTime = 600; // [sec]
        public GreetingProviderImpl GreetingProviderImpl { get; set; }
        public SiLA2Server SiLAServer { get; set; }
        public Feature SiLAFeature { get; set; }

        #endregion

        #region Constructors and destructors

        public TemperatureControllerImpl()
        {
            // start temperature simulation
            BackgroundWorker temperatureSimulator = new BackgroundWorker
            {
                WorkerReportsProgress = false,
                WorkerSupportsCancellation = false
            };

            temperatureSimulator.DoWork += new DoWorkEventHandler(temperatureSimulator_DoWork);
            temperatureSimulator.RunWorkerAsync();
        }

        #endregion

        #region Overrides of TemperatureControllerBase

        #region Command 'ControlTemperature'

        /// <summary>Triggers the TemperatureControl command execution.</summary>
        public override Task<SiLAFramework.CommandConfirmation> ControlTemperature(ControlTemperature_Parameters request, ServerCallContext context)
        {
            Log.Debug($"\n\"{new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            // check temperature parameter against constraints from feature definition
            Validation.ValidateParameter(request.TargetTemperature.Value, SiLAFeature, ControlTemperatureCmdId, "TargetTemperature");

            // check if greeting has been done already (no real use case but demonstrating the interaction between two features via the SiLA2Server)
            if (!GreetingProviderImpl.GreetingHappened)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardExecutionError("NoGreetingError",
                        "Temperature controlling can't start without greeting ;)",
                        "Call the greeting command and try again"));
            }

            // add new command
            var executionId = SiLAServer.CommandExecutionManager.AddObservableCommandExecution(ControlTemperatureCmdId, _executionTime);

            // start command
            new Thread(() =>
            {
                DoTemperatureControl(executionId, request.TargetTemperature.Value);
            }).Start();

            return Task.FromResult(new SiLAFramework.CommandConfirmation
            {
                CommandId = new SiLAFramework.CommandExecutionUUID
                {
                    CommandId = executionId.ToString()
                }
                // TODO: add LifeTimeOfResponse
            });
        }

        /// <summary>Monitors the execution state of the command execution assigned to the given command execution ID.</summary>
        public override async Task ControlTemperature_State(SiLAFramework.CommandExecutionUUID request, IServerStreamWriter<SiLAFramework.ExecutionInfo> responseStream, ServerCallContext context)
        {
            Log.Debug($"\n\"{new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name}\" request received");

            var commandId = Guid.Parse(request.CommandId);
            // check command ID
            SiLAServer.CommandExecutionManager.CheckCommandExecutionId(commandId, ControlTemperatureCmdId);

            var executionInfo = SiLAServer.CommandExecutionManager.GetObservableCommandExecutionInfo(commandId);

            // check if target temperature has been reached
            while (executionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.Waiting ||
                   executionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.Running)
            {
                await responseStream.WriteAsync(executionInfo);
                await Task.Delay(500);
            }

            await responseStream.WriteAsync(executionInfo);
        }

        /// <summary>Gets the result of the TemperatureControl command execution assigned to the given command execution ID.</summary>
        public override Task<ControlTemperature_Responses> ControlTemperature_Result(SiLAFramework.CommandExecutionUUID request, ServerCallContext context)
        {
            Log.Debug($"\n\"{new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            const string commandIdentifier = "ControlTemperature";

            var commandId = Guid.Parse(request.CommandId);
            // check command ID
            SiLAServer.CommandExecutionManager.CheckCommandExecutionId(commandId, commandIdentifier);

            // check if command execution has been finished
            var commandStatus = SiLAServer.CommandExecutionManager.GetObservableCommandExecutionInfo(commandId).CommandStatus;
            if (commandStatus != SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully &&
                commandStatus != SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardFrameworkError(
                    SiLAFramework.FrameworkError.Types.ErrorType.CommandExecutionNotFinished));
            }
            
            // todo report ControlInterrupted error in result if it happen
            
            return Task.FromResult(new ControlTemperature_Responses());
        }

        /// <summary>
        /// Does the temperature control:
        ///  - sets the required target temperature
        ///  - waits for target temperature has been reached or an error occured meanwhile
        ///  - sets the command execution state assigned to the given command execution ID
        /// </summary>
        /// <param name="executionID">The ID of the command execution.</param>
        /// <param name="targetTemperature">The target temperature to be reached.</param>
        private void DoTemperatureControl(Guid executionID, double targetTemperature)
        {
            // set command state to running
            SiLAFramework.ExecutionInfo executionInfo = SiLAServer.CommandExecutionManager.GetObservableCommandExecutionInfo(executionID);
            executionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.Running;

            // set new target temperature
            _targetTemperature = targetTemperature;

            // persist start temperature
            double startTemperature = _currentTemperature;

            // while target temperature has not been reached
            while (Math.Abs(_currentTemperature - targetTemperature) > _temperatureTolerance)
            {
                // check if target temperature has been changed by another command call
                if (Math.Abs(_targetTemperature - targetTemperature) > _temperatureTolerance)
                {
                    // set current execution state to failed
                    executionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError;
                }

                // update progress and remaining time
                double progress = Math.Abs(_currentTemperature - startTemperature) / Math.Abs(targetTemperature - startTemperature);
                executionInfo.ProgressInfo = new SiLAFramework.Real { Value = progress };

                double remainingTime = Math.Abs(_currentTemperature - targetTemperature) * 2;
                executionInfo.EstimatedRemainingTime = new SiLAFramework.Duration
                {
                    Seconds = (long)remainingTime,
                    Nanos = (int)((remainingTime - Math.Floor(remainingTime)) * Math.Pow(10, 9))
                };
            }

            executionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully;
            executionInfo.ProgressInfo = new SiLAFramework.Real
            {
                Value = 1
            };
            executionInfo.EstimatedRemainingTime = new SiLAFramework.Duration
            {
                Seconds = 0,
                Nanos = 0
            };
        }

        #endregion

        #region Property 'CurrentTemperature'

        /// <summary>
        /// Returns the current temperature with a frequency of 1 Hz.
        /// </summary>
        /// <param name="request">The gRPC parameters (empty)</param>
        /// <param name="responseStream">The response stream.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns></returns>
        public override async Task Subscribe_CurrentTemperature(Subscribe_CurrentTemperature_Parameters request, IServerStreamWriter<Subscribe_CurrentTemperature_Responses> responseStream, ServerCallContext context)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name;
            Log.Debug($"\n\"{methodName}\" request received");

            do
            {
                await responseStream.WriteAsync(new Subscribe_CurrentTemperature_Responses
                {
                    CurrentTemperature = new SiLAFramework.Real
                    {
                        Value = _currentTemperature
                    }
                });
                Thread.Sleep(1000);
            } while (!context.CancellationToken.IsCancellationRequested);

            Log.Warn($"\n\"{methodName}\" subscription cancelled");
        }

        #endregion

        #endregion

        /// <summary>
        /// The temperature simulation routine. Changes the temperature randomly around the current value or towards a given target temperature.
        /// </summary>
        private void temperatureSimulator_DoWork(object sender, DoWorkEventArgs e)
        {
            // use the current temperature as target since no target has been set
            _targetTemperature = _currentTemperature;

            Random rnd = new Random();
            while (true)
            {
                // if target has to be reached (difference to current temperature is outside the tolerance) use stronger temperature change
                double temperatureChange;
                if (Math.Abs(_targetTemperature - _currentTemperature) > _temperatureTolerance)
                {
                    temperatureChange = rnd.NextDouble() * 0.1;

                    if (_targetTemperature < _currentTemperature)
                    {
                        temperatureChange *= -1;
                    }
                }
                else
                {
                    temperatureChange = rnd.NextDouble() * 0.5 - 0.5;
                }

                _currentTemperature += temperatureChange;
                Thread.Sleep(100);
            }
        }
    }
}