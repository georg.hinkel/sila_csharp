using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using NLog.Targets;
using Xunit.Abstractions;
using Common.Logging;
using Common.Logging.Configuration;

namespace test.Utils
{
    //@TODO: remove this as this is a duplicate of the sila2.Utils in sila_implementations
    public static class Logging
    {
        private static string GetAssemblyPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        private static IConfiguration LoadConfigXml(string fileName)
        {
            return new ConfigurationBuilder()
                .SetBasePath(GetAssemblyPath())
                .AddXmlFile(fileName, optional: true, reloadOnChange: false)
                .Build();
        }

        /// <summary>
        /// Configures the applications logging, to consume Common.Logging abstractins
        /// as specified by the config file
        /// </summary>
        public static void SetupCommonLogging(string configFile = "common_logging.config")
        {
            var config = LoadConfigXml(configFile);
            var logConfiguration = new LogConfiguration();
            config.GetSection("common:logging").Bind(logConfiguration);
            Common.Logging.LogManager.Configure(logConfiguration);
        }

        /// <summary>
        /// Forward logs to unit test output
        /// </summary>
        /// <param name="output">The output facade</param>
        public static void SetupLogForwarding(ITestOutputHelper output)
        {
            MethodCallTarget target = new MethodCallTarget("SiLATester", (logEvent, parms) =>
            {
                output.WriteLine(logEvent.FormattedMessage);
            });
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, NLog.LogLevel.Trace);
        }
    }
}