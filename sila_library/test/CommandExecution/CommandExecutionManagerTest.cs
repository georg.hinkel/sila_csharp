using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using test.Utils;
using Grpc.Core;
using sila2;
using Xunit;
using Xunit.Abstractions;

namespace CommandExecution
{
    public class CommandExecutionManagerTest
    {
        private readonly ITestOutputHelper output;

        public CommandExecutionManagerTest(ITestOutputHelper output)
        {
            Logging.SetupCommonLogging();
            this.output = output;
            Logging.SetupLogForwarding(output);
        }

        [Fact]
        public void TestCommandCleanup()
        {
            CommandExecutionManager manager = new CommandExecutionManager(1);
            var commandId = manager.AddObservableCommandExecution("test", 2, 3);
            var command = getCommand(manager, commandId);
            var maxCommandLifeTime = command.MaxCommandLifeTime;
            manager.OnCommandRemoved += (id, cmd) =>
            {
                var delta = DateTime.Now.Subtract(maxCommandLifeTime).TotalSeconds;
                Assert.True(delta > 0.0 && delta <= 1.0);
                output.WriteLine($"Command {id} removed!");
            };
            Thread.Sleep(3000);
            manager.GetObservableCommandExecutionInfo(commandId);
            manager.CheckCommandExecutionId(commandId, command.CommandIdentifier);
            Thread.Sleep(3000);
            Assert.Throws<RpcException>(() =>
            {
                manager.GetObservableCommandExecutionInfo(commandId);
            });
            Assert.Throws<RpcException>(() =>
            {
                manager.CheckCommandExecutionId(commandId, command.CommandIdentifier);
            });
        }
        
        [Fact]
        public void TestRemoveEntry()
        {
            CommandExecutionManager manager = new CommandExecutionManager(100);
            Guid guid = new Guid();
            manager.RemoveCommandExecution(guid);
            var commandId = manager.AddObservableCommandExecution("test", 20, 20);
            var command = getCommand(manager, commandId);
            manager.GetObservableCommandExecutionInfo(commandId);
            manager.CheckCommandExecutionId(commandId, command.CommandIdentifier);
            manager.RemoveCommandExecution(commandId);
            Assert.Throws<RpcException>(() =>
            {
                manager.GetObservableCommandExecutionInfo(commandId);
            });
            Assert.Throws<RpcException>(() =>
            {
                manager.CheckCommandExecutionId(commandId, command.CommandIdentifier);
            });
        }
        
        [Fact]
        public void TestObservableCommandConstructor()
        {
            ObservableCommandExecution cmd;
            Assert.Throws<ArgumentException>(() =>
            {
                cmd = new ObservableCommandExecution(null, -1, 0);
            });
            cmd = new ObservableCommandExecution("MyCommandIdentifier", 2, 2);
            Assert.True(cmd.MinCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 2.0);
            Assert.True(cmd.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 4.0);
            cmd = new ObservableCommandExecution("MyCommandIdentifier", 0, 1);
            Assert.True(cmd.MinCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 0.0);
            Assert.True(cmd.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 1.0);
            cmd = new ObservableCommandExecution("MyCommandIdentifier", 0, 0);
            Assert.True(cmd.MinCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 0.0);
            Assert.True(cmd.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 0.0);
            cmd = new ObservableCommandExecution("MyCommandIdentifier", 1, 0);
            Assert.True(cmd.MinCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 1.0);
            Assert.True(cmd.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 1.0);
            cmd = new ObservableCommandExecution("MyCommandIdentifier", -1, 1);
            Assert.Equal(cmd.MinCommandLifeTime, DateTime.MaxValue);
            Assert.Equal(cmd.MaxCommandLifeTime, DateTime.MaxValue);
            cmd = new ObservableCommandExecution("MyCommandIdentifier", -1, 0);
            Assert.Equal(cmd.MinCommandLifeTime, DateTime.MaxValue);
            Assert.Equal(cmd.MaxCommandLifeTime, DateTime.MaxValue);
            cmd = new ObservableCommandExecution("Neo", 1, 01);
            Assert.True(cmd.MinCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 1.0);
            Assert.True(cmd.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 2.0);
        }

        private static ObservableCommandExecution getCommand(CommandExecutionManager manager, Guid commandId)
        {
            Dictionary<Guid, ObservableCommandExecution> commands = (Dictionary<Guid, ObservableCommandExecution>)manager
                .GetType()
                .GetField("_commandExecutions", BindingFlags.Instance | BindingFlags.NonPublic)
                .GetValue(manager);
            if (commands.TryGetValue(commandId, out var command))
            {
                return command;
            }
            return null;
        }
    }
}