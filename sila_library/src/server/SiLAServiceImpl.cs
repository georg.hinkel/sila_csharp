﻿namespace sila2
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using Grpc.Core;
    using SiLAFramework = Sila2.Org.Silastandard;
    using Sila2.Org.Silastandard.Core.Silaservice.V1;
    using server_config;
    using Common.Logging;

    public class SiLAServiceImpl : SiLAService.SiLAServiceBase, IFeatureImplementation
    {
        private static ILog _Logger = LogManager.GetLogger<SiLAServiceImpl>();
        public IServerConfigWrapper ServerConfigWrapper { get; set; }
        public ServerInformation ServerInfo { get; set; }

        #region Implementation of IFeatureImplementation
        public Feature SiLAFeature { get; set; }
        public SiLA2Server SiLAServer { get; set; }
        #endregion

        #region Overrides of SiLAServiceBase
        public override Task<GetFeatureDefinition_Responses> GetFeatureDefinition(GetFeatureDefinition_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request -> ");

            // lookup feature by given qualified identifier
            if (!this.SiLAServer.ImplementedFeatures.Contains(request.QualifiedFeatureIdentifier.FeatureIdentifier.Value))
            {
                // get error identifier and message from standard execution error specified in the feature definition (SiLAService Feature)
                FeatureStandardExecutionError error = SiLAFeature.GetStandardExecutionErrors().Find(match => match.Identifier == "UnimplementedFeature");
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardExecutionError(error.Identifier,
                    "Feature definition is probably missing",
                    "Check that the requested feature definition is valid and available"));
            }

            // get feature from server
            Feature feature = this.SiLAServer.GetFeature(request.QualifiedFeatureIdentifier.FeatureIdentifier.Value);

            // serialize Feature definition
            var serializer = new XmlSerializer(typeof(Feature));
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new ExtentedStringWriter(stringBuilder, new UTF8Encoding(false)))
            {
                serializer.Serialize(stringWriter, feature);
            }

            return Task.FromResult(new GetFeatureDefinition_Responses { FeatureDefinition = new DataType_FeatureDefinition { FeatureDefinition = new SiLAFramework.String { Value = stringBuilder.ToString().Replace(Environment.NewLine, string.Empty) } } });
        }

        public override Task<SetServerName_Responses> SetServerName(SetServerName_Parameters request, ServerCallContext context)
        {
            var config = ServerConfigWrapper.GetConfig();

            _Logger.Info($"Request ->  Response = \"{request.ServerName}\"");

            config.Name = request.ServerName.Value;
            ServerConfigWrapper.SetConfig(config);

            return Task.FromResult(new SetServerName_Responses { });
        }

        public override Task<Get_ServerName_Responses> Get_ServerName(Get_ServerName_Parameters request, ServerCallContext context)
        {

            ServerConfig config = null;
            try
            {
                config = ServerConfigWrapper.GetConfig();
                _Logger.Info($"Request ->  Response = \"{config.Name}\"");
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }
            return Task.FromResult(new Get_ServerName_Responses { ServerName = new SiLAFramework.String { Value = config.Name } });
        }

        public override Task<Get_ServerUUID_Responses> Get_ServerUUID(Get_ServerUUID_Parameters request, ServerCallContext context)
        {
            var config = ServerConfigWrapper.GetConfig();
            _Logger.Trace($"Request ->  Response = \"{config.Uuid}\"");

            return Task.FromResult(new Get_ServerUUID_Responses { ServerUUID = new SiLAFramework.String { Value = config.Uuid.ToString() } });
        }

        public override Task<Get_ServerType_Responses> Get_ServerType(Get_ServerType_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request ->  Response = \"{ServerInfo.Type}\"");

            return Task.FromResult(new Get_ServerType_Responses { ServerType = new SiLAFramework.String { Value = ServerInfo.Type } });
        }

        public override Task<Get_ServerDescription_Responses> Get_ServerDescription(Get_ServerDescription_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request ->  Response = \"{ServerInfo.Description}\"");

            return Task.FromResult(new Get_ServerDescription_Responses { ServerDescription = new SiLAFramework.String { Value = ServerInfo.Description } });
        }

        public override Task<Get_ServerVendorURL_Responses> Get_ServerVendorURL(Get_ServerVendorURL_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request ->  Response = \"{ServerInfo.VendorURI}\"");

            return Task.FromResult(new Get_ServerVendorURL_Responses { ServerVendorURL = new DataType_URL { URL = new SiLAFramework.String { Value = ServerInfo.VendorURI } } });
        }

        public override Task<Get_ServerVersion_Responses> Get_ServerVersion(Get_ServerVersion_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request ->  Response = \"{ServerInfo.Version}\"");

            return Task.FromResult(new Get_ServerVersion_Responses { ServerVersion = new SiLAFramework.String { Value = ServerInfo.Version } });
        }

        public override Task<Get_ImplementedFeatures_Responses> Get_ImplementedFeatures(Get_ImplementedFeatures_Parameters request, ServerCallContext context)
        {
            _Logger.Info($"Request -> ");

            Get_ImplementedFeatures_Responses response = new Get_ImplementedFeatures_Responses();
            foreach (var f in this.SiLAServer.ImplementedFeatures)
            {
                response.ImplementedFeatures.Add(new DataType_FeatureIdentifier { FeatureIdentifier = new SiLAFramework.String { Value = f } });
            }

            return Task.FromResult(response);
        }
        private sealed class ExtentedStringWriter : StringWriter
        {
            public ExtentedStringWriter(StringBuilder builder, Encoding desiredEncoding)
                : base(builder)
            {
                this.Encoding = desiredEncoding;
            }

            public override Encoding Encoding { get; }
        }

        #endregion
    }
}