﻿namespace sila2
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Common.Logging;
    using Grpc.Core;
    using server_config;
    using System.Net;
    using System.Net.Sockets;
    using SiLAFramework = Sila2.Org.Silastandard;
    using SiLAService = Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService;

    /// <summary>
    /// Base class for all SiLA 2 Servers.
    /// </summary>
    public class SiLA2Server
    {
        private static readonly ILog _Logger = LogManager.GetLogger<SiLA2Server>();

        #region Members

        /// <summary>The gRPC server.</summary>
        public Server GrpcServer { get; }

        /// <summary>Manage observable command executions</summary>
        public CommandExecutionManager CommandExecutionManager { get; }

        /// <summary>The list of implemented features.</summary>
        private readonly List<Feature> _implementedFeatures;

        /// <summary>Discovery listener</summary>
        private readonly Discovery.SiLAListener _listener = new Discovery.SiLAListener();
        #endregion
        
        #region Utils

        /// <summary>
        /// Check if the specified TCP port is available
        /// If the port is already in use, an SocketException is thrown
        /// </summary>
        private static void portAvailableOrThrow(int port)
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Parse("0.0.0.0"), port);
            tcpListener.Start();
            tcpListener.Stop();
        }
        
        #endregion

        #region Constructors and destructors

        /// <summary>
        /// Creates a new instance of the SiLA2Server class, adds the SiLAService Feature implemenatation, creates a gRPC Server and registeres the server for Server discovery.
        /// </summary>
        /// <param name="serverInformation">structure containing server information</param>
        /// <param name="portNumber">The port number the server is listening at.</param>
        /// <param name="configFile">Configuration file where server config is to be located</param>
        public SiLA2Server(ServerInformation serverInformation, int portNumber, string configFile = null)
        {
            // C# gRPC does not check if the port is already in use, so we check it manually
            portAvailableOrThrow(portNumber);
            
            IServerConfigWrapper serverConfigWrapper;
            var defaultConfig = new ServerConfig(serverInformation.Type, Guid.NewGuid());
            if (configFile == null)
            {
                serverConfigWrapper = new NonPersistentServerConfigWrapper(defaultConfig);
            }
            else
            {
                serverConfigWrapper = new PersistentServerConfigWrapper(configFile, defaultConfig);
            }
            this.CommandExecutionManager = new CommandExecutionManager();
            this._implementedFeatures = new List<Feature>();

            // create gRPC server
            GrpcServer = new Server
            {
                Ports = { new ServerPort("0.0.0.0", portNumber, ServerCredentials.Insecure) }
            };

            // add mandatory SiLAService feature
            ////"https://gitlab.com/SiLA2/sila_base/raw/master/feature_definitions/org/sila-standard/core/SiLAService.xml"
            Feature feature = ReadFeature("sila2.features.SiLAService.xml", typeof(SiLAServiceImpl));
            var config = serverConfigWrapper.GetConfig();
            // add service to gRPC server
            GrpcServer.Services.Add(SiLAService.BindService(new SiLAServiceImpl { SiLAServer = this, SiLAFeature = feature, ServerConfigWrapper = serverConfigWrapper, ServerInfo = serverInformation }));
            try
            {
                // check if the choosen name for Service Discovery causes no naming conflicts
                string instanceName = $"{config.Uuid}";
                string hostName = serverInformation.Type.Replace(' ', '-');

                // register for Server Discovery
                _listener.StartListening(instanceName, hostName, (ushort)portNumber);
            }
            catch (Exception e)
            {
                // TODO: should we ignore SiLA Server Discovery errors for now?
                _Logger.Error("Ignored Exception:", e);
            }
        }
        #endregion

        #region gRPC server

        public void StartServer()
        {
            _Logger.Info("Starting server");
            GrpcServer.Start();
        }

        public void ShutdownServer()
        {
            _listener.Stop();
            CommandExecutionManager.Stop();
            _Logger.Info("Shutting down Grpc server");
            GrpcServer.KillAsync().Wait();
        }

        #endregion

        #region Feature discovery

        /// <summary>
        /// Method for extracting a <see cref="Feature"/> from an EmbeddedResource.
        /// </summary>
        /// <param name="resourceName">The name of the embedded resource.
        ///
        /// The pattern is: '[assembly default namespace].[directory].[filename]'
        /// For example: 'sila2.features.SiLAService.xml'.
        /// </param>
        /// <param name="implementationType">A type from the assembly that contains the EmbeddedResource.</param>
        /// <exception cref="FileNotFoundException">Thrown when the embedded resource stream can't be found.</exception>
        /// <exception cref="ApplicationException">Thrown when reading the feature from the Stream fails.</exception>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string resourceName, Type implementationType)
        {
            // get the embedded resource from the assembly of the implementation
            Stream featureStream = implementationType.Module.Assembly.GetManifestResourceStream(resourceName);
            if (featureStream == null)
            {
                throw new FileNotFoundException($"Feature resource '{resourceName}' was not found" +
                    $" in the list of embedded resources: [{string.Join(", ", implementationType.Module.Assembly.GetManifestResourceNames())}]." +
                    " Did you set its BuildAction to 'EmbeddedResource'?");
            }

            try
            {
                var feature = FeatureGenerator.ReadFeatureFromStream(featureStream);
                _implementedFeatures.Add(feature);
                return feature;
            }
            catch (Exception e)
            {
                _Logger.Error("Unable to parse Xml from embedded resource stream.", e);
                throw new ApplicationException("Unable to parse Xml from embedded resource stream.", e);
            }
        }

        /// <summary>
        /// Reads and deserializes the feature definition from the given file.
        /// (it simply does the call 'GrpcServer.Services.Add([grpc_stub_class_name].BindService(new [feature_implementation_class_name(this, [implemented_feature_object])))' )
        /// </summary>
        /// <param name="featureDefinitionFile">The file containing the XML Feature Defintion.</param>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string featureDefinitionFile)
        {
            // deserialize the given feature defintion
            Feature feature;
            if (Uri.IsWellFormedUriString(featureDefinitionFile, UriKind.Absolute))
            {
                feature = FeatureGenerator.ReadFeatureFromOnlineResource(featureDefinitionFile);
            }
            else
            {
                feature = FeatureGenerator.ReadFeatureFromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, featureDefinitionFile));
            }

            _implementedFeatures.Add(feature);

            return feature;
        }

        /// <summary>
        /// Gets the feature object with the given feature identifier.
        /// </summary>
        /// <param name="featureIdentifier">The (full quallified) feature identifier.</param>
        /// <returns>The feature object.</returns>
        public Feature GetFeature(string featureIdentifier)
        {
            return _implementedFeatures.First(f => f.FullyQualifiedIdentifier == featureIdentifier);
        }

        /// <summary>
        /// Gets a list of fully quallified identifiers of the implemented features.
        /// </summary>
        public List<string> ImplementedFeatures
        {
            get { return _implementedFeatures.Select(feature => feature.FullyQualifiedIdentifier).ToList(); }
        }

        #endregion

        #region Test state stuff

        /// <summary>
        /// Alernative way to add a feature implmenatation (using reflection).
        /// Reads the feature definition, constrcuts the implementation instance with initial properties, creates the gRPC service and adds it to the gRPC server.
        /// -> it simply does the call 'GrpcServer.Services.Add([grpc_stub_class_name].BindService(new [feature_implementation_class_name]{ SiLAFeature = [implemented_feature_object], SiLAServer = this)))'
        /// </summary>
        /// <param name="feature">The Feature object</param>
        /// <param name="grpcStub">The type of the gRPC stub of the feature</param>
        /// <param name="implementation">The feature implmentation type -> has to implement the IFeatureImplementation interface.</param>
        /// <returns></returns>
        public Feature AddFeatureImplementationByReflection(Feature feature, Type grpcStub, Type implementation)
        {
            // check if IFeatureImplementation interface has been implemented
            if (!implementation.GetInterfaces().Contains(typeof(IFeatureImplementation)))
            {
                throw new Exception("Given feature implementation type does not implement the IFeatureImplementation interface");
            }

            // instantiate the implementation class
            ConstructorInfo ci = implementation.GetConstructor(new Type[] { });
            if (ci == null)
            {
                throw new ApplicationException("Unable to get constructor of the feature implementation " + implementation);
            }

            object impl = ci.Invoke(new object[] { });

            // set implementation members
            implementation.InvokeMember("SiLAFeature", BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, impl, new[] { impl });
            implementation.InvokeMember("SiLAServer", BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, impl, new object[] { this });

            // invoke the BindService method
            ServerServiceDefinition service = (ServerServiceDefinition)grpcStub.InvokeMember("BindService", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, null, null, new[] { impl });

            // add service to gRPC server
            GrpcServer.Services.Add(service);

            _implementedFeatures.Add(feature);

            return feature;
        }

        #endregion
    }
}