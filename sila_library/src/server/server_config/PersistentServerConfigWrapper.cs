namespace sila2.server_config
{
    using System;
    using System.IO;
    using Common.Logging;
    using System.Runtime.Serialization.Json;

    /// <summary>
    /// Class responsible for reading and writing ServerConfigs to a specified file
    /// </summary>
    public class PersistentServerConfigWrapper : IServerConfigWrapper
    {
        private static ILog log = LogManager.GetLogger<PersistentServerConfigWrapper>();
        private static Object fileLock = new Object();
        private string dbFile;
        private ServerConfig defaultConfig;
        private DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ServerConfig));

        /// <summary>
        /// Constructor 
        /// </summary>
        public PersistentServerConfigWrapper(string configFile, ServerConfig defaultConfig)
        {
            SetDb(configFile);
            try
            {
                ReadConfiguration();
            }
            catch (Exception e)
            {
                log.Info($"Creating default ServerConfig");
                SetConfig(defaultConfig);
            }
        }

        /// <summary>
        /// Writes the specified config into the db
        /// </summary>
        public void SetConfig(ServerConfig config)
        {
            lock (fileLock)
            {
                using (var fileStream = File.Create(dbFile))
                {
                    serializer.WriteObject(fileStream, config);
                }
            }
            log.Info($"Updating Server configuration Name: {config.Name} and UUID: {config.Uuid}");
        }

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns></returns>
        public ServerConfig GetConfig()
        {
            lock (fileLock)
            {
                var config = ReadConfiguration();
                if (config == null)
                {
                    throw new InvalidOperationException("Cannot get chache, must call readConfiguration first!");
                }
                return config;
            }
        }

        /// <summary>
        /// Loads the server configuration from the db i.e. file
        /// </summary>
        private ServerConfig ReadConfiguration()
        {
            using (var fileStream = File.Open(dbFile, FileMode.Open))
            {
                var config = serializer.ReadObject(fileStream) as ServerConfig;
                if (config.Uuid == null || config.Name == null)
                {
                    throw new Exception("Empty configuration");
                }
                return config;
            }
        }

        /// <summary>
        /// Creates a database for the specified file if not already created
        /// </summary>
        private void SetDb(string filename)
        {
            CreateFile(filename);
        }

        /// <summary>
        /// Create a file if does not exist already and populate it with an empty JSON object
        /// </summary>
        /// <param name="filename">name of file to create or check</param>
        private void CreateFile(string filename)
        {
            dbFile = Path.GetFullPath(filename);
            if (!File.Exists(dbFile))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(dbFile));
                using (var fileStream = File.Create(dbFile))
                {
                    using (var streamWriter = new StreamWriter(fileStream))
                    {
                        streamWriter.Write("{}");
                    }
                }
                log.Info($"Creating server configuration file: {dbFile}");
                return;
            }
            log.Info($"Config file already exists: {dbFile}");
        }
    }
}