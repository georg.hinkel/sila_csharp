namespace sila2.server_config
{
    using System;
    using Common.Logging;
    using System.Runtime.Serialization.Json;

    /// <summary>
    /// Class responsible for reading and writing ServerConfigs to local in process cache
    /// Non persistent over multiple server lifetimes
    /// </summary>
    public class NonPersistentServerConfigWrapper : IServerConfigWrapper
    {
        private static readonly ILog log = LogManager.GetLogger<NonPersistentServerConfigWrapper>();
        private ServerConfig cache;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config">config to load into the cache</param>
        public NonPersistentServerConfigWrapper(ServerConfig config)
        {
            cache = config;
        }

        /// <summary>
        /// Writes the specified config into the db
        /// </summary>
        /// <param name="config"></param>
        public void SetConfig(ServerConfig config)
        {
            log.Info($"Updating Server configuration: {config}");
            cache = config;
        }

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns></returns>
        public ServerConfig GetConfig()
        {
            if (cache == null)
            {
                throw new InvalidOperationException("Cannot get chache, must call readConfiguration first!");
            }
            log.Trace($"config: {cache}");
            return cache;
        }
    }
}