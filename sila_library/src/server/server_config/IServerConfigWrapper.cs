namespace sila2.server_config
{
    /// <summary>
    /// Interface used to implement access layers to a given server configuration
    /// </summary>
    public interface IServerConfigWrapper
    {
        /// <summary>
        /// Set the ServerConfig
        /// </summary>
        void SetConfig(ServerConfig config);

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns>Server Configuration</returns>
        ServerConfig GetConfig();
    }
}