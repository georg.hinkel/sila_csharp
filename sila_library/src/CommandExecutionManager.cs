namespace sila2
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Common.Logging;
    using SiLAFramework = Sila2.Org.Silastandard;

    public class CommandExecutionManager
    {
        public delegate void CommandRemovedEvent(Guid id, ObservableCommandExecution cmd);
        private static ILog _Logger = LogManager.GetLogger<CommandExecutionManager>();
        /// <summary>
        /// Triggered when a command execution is removed and will not be valid anymore
        /// </summary>
        public event CommandRemovedEvent OnCommandRemoved;

        private const int DefaultCleanupMsCheckInterval = 60 * 1000; // 1 minute
        /// <summary>All current observable command executions, with the identifying CommandExecutionUUID, the command identifier and the current execution state.</summary>
        private readonly Dictionary<Guid, ObservableCommandExecution> _commandExecutions = new Dictionary<Guid, ObservableCommandExecution>();
        private readonly object _executionsLock = new object();
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        /// <summary>
        ///
        /// </summary>
        /// <param name="cleanCheckMsInterval">Number of milliseconds between each clean check</param>
        public CommandExecutionManager(int cleanCheckMsInterval = DefaultCleanupMsCheckInterval)
        {
            new Thread(() =>
            {
                while (_cancellationTokenSource.Token.IsCancellationRequested == false)
                {
                    lock (_executionsLock)
                    {
                        // todo we can optimize this by adding command execution `without` life time in a separate list for example
                        foreach (var cmd in _commandExecutions.Reverse())
                        {
                            // If we reach the maximum time the command can be alive
                            if (cmd.Value.MaxCommandLifeTime.Subtract(DateTime.Now).TotalSeconds <= 0.0)
                            {
                                _commandExecutions.Remove(cmd.Key);
                                OnCommandRemoved?.Invoke(cmd.Key, cmd.Value);
                            }
                        }
                    }
                    _cancellationTokenSource.Token.WaitHandle.WaitOne(cleanCheckMsInterval);
                }
            }).Start();
            _Logger.Info("Created Command Execution Manager");
        }

        /// <summary>
        /// Stop the cleanup of command execution thread
        /// This function should not be called by implementers
        /// </summary>
        public void Stop()
        {
            this._cancellationTokenSource.Cancel();
        }

        ~CommandExecutionManager()
        {
            Stop();
        }

        /// <summary>
        /// Adds a new observable command execution with the given command identifier and generates new command execution ID for it.
        /// The caller is expected to remove the command execution if no commandLifeTime is specified
        /// when he doesn't need it anymore by calling <see cref="RemoveCommandExecution"/>
        /// The caller should also update the command execution state <see cref="GetObservableCommandExecutionInfo"/>
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command this execution is associated with.</param>
        /// <param name="commandLifeTime">
        /// The minimum amount of time the command execution identifier will be valid in seconds
        /// Specifying a negative value will make the command available for the lifetime of the server.
        /// </param>
        /// <param name="deletionTimeDelay">
        /// The delay in seconds to wait after commandLifeTime seconds
        /// have elapsed to set the command execution identifier as invalid
        /// </param>
        public Guid AddObservableCommandExecution(
            string commandIdentifier,
            long commandLifeTime = -1,
            ulong deletionTimeDelay = 0)
        {
            lock (_executionsLock)
            {
                var guid = Guid.NewGuid();
                _commandExecutions.Add(guid, new ObservableCommandExecution(commandIdentifier, commandLifeTime, deletionTimeDelay));
                return guid;
            }
        }


        /// <summary>Checks if the given command execution ID is valid.</summary>
        /// <param name="commandExecutionId">The command execution ID to be checked.</param>
        /// <param name="commandIdentifier">The identifier of the command this ID shall be used for.</param>
        public void CheckCommandExecutionId(Guid commandExecutionId, string commandIdentifier)
        {
            lock (_executionsLock)
            {
                if (_commandExecutions.TryGetValue(commandExecutionId, out var command))
                {
                    if (command.CommandIdentifier == commandIdentifier)
                    {
                        return;
                    }
                }
                // if command execution ID hasn't been found or does not match with command id -> raise SiLA Framework Error
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardFrameworkError(
                    SiLAFramework.FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid));
            }
        }

        /// <summary>
        /// Returns the execution state of the command execution assigned to the given command execution ID
        /// or raises a FrameworkError if the given ID is not valid.
        /// </summary>
        public SiLAFramework.ExecutionInfo GetObservableCommandExecutionInfo(Guid commandExecutionId)
        {
            lock (_executionsLock)
            {
                if (_commandExecutions.TryGetValue(commandExecutionId, out var command))
                {
                    return command.CurrentState;
                }
            }
            // if command execution ID hasn't been found -> raise SiLA Framework Error
            ErrorHandling.raiseRPCError(ErrorHandling.GetStandardFrameworkError(
                SiLAFramework.FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid));
            return null;
        }

        /// <summary>Remove the command execution from the executions list.</summary>
        /// <param name="commandExecutionId">The command execution ID to be checked.</param>
        public void RemoveCommandExecution(Guid commandExecutionId)
        {
            lock (_executionsLock)
            {
                if (_commandExecutions.TryGetValue(commandExecutionId, out var command))
                {
                    _commandExecutions.Remove(commandExecutionId);
                    OnCommandRemoved?.Invoke(commandExecutionId, command);
                }
            }

        }
    }
}