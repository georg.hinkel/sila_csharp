﻿namespace sila2
{
    public interface IFeatureImplementation
    {
        Feature SiLAFeature { get; set; }

        SiLA2Server SiLAServer { get; set; }
    }
}