﻿using Google.Protobuf.WellKnownTypes;

namespace sila2
{
    using System;
    using Google.Protobuf;
    using Grpc.Core;
    using SiLAFramework = Sila2.Org.Silastandard;

    /// <summary>
    /// Provides methods to generate SiLA errors and raise gRPC exceptions and to handle those execptions.
    /// </summary>
    public class ErrorHandling
    {    
        public static void raiseRPCError(SiLAFramework.SiLAError silaError) {
            throw new RpcException(
                new Status(StatusCode.Aborted,
                    silaError.ToByteString().ToStringUtf8()
                )
            );
        }
        public static SiLAFramework.SiLAError GetStandardExecutionError(string errorIdentifier, string cause, string action = "")
        {
            return new SiLAFramework.SiLAError
            {
                ExecutionError = new SiLAFramework.ExecutionError()
                {
                    ErrorIdentifier = errorIdentifier,
                    Cause = cause,
                    Action = action
                }
            };
        }
        
        public static SiLAFramework.SiLAError GetStandardValidationError(string parameter, string cause, string action = "")
        {
            return new SiLAFramework.SiLAError
            {
                ValidationError = new SiLAFramework.ValidationError()
                {
                    Parameter = parameter,
                    Cause = cause,
                    Action = action
                }
            };
        }
        
        public static SiLAFramework.SiLAError GetStandardFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType errorType)
        {
            return new SiLAFramework.SiLAError
            {
                FrameworkError = new SiLAFramework.FrameworkError()
                {
                    ErrorType = errorType
                }
            };
        }

        public static string HandleException(Exception ex)
        {
            RpcException rpcEx = ex is RpcException exception ? exception : ex.InnerException as RpcException;
            if (rpcEx != null)
            {
                if (rpcEx.Status.StatusCode == StatusCode.Aborted && !string.IsNullOrEmpty(rpcEx.Status.Detail))
                {
                    // check if SiLAError object is contained
                    try
                    {
                        var error = retrieveSiLAError(rpcEx);
                        return $"SiLA {getSiLAErrorType(error)} Error occured. Details: {error}";
                    }
                    catch (Exception)
                    {
                        // any other gRPC error => SiLA Connection Error
                        return "SiLA Connection Error occured: " + rpcEx.Message;
                    }
                }
                else
                {
                    return "Non-SiLA gRPC exception occured: " + rpcEx.Message;
                }
            }

            return "Exception occured: " + ex.Message;
        }

        public static SiLAFramework.SiLAError retrieveSiLAError(RpcException e)
        {
           return SiLAFramework.SiLAError.Parser.ParseFrom(ByteString.CopyFromUtf8(e.Status.Detail));
        }
        
        private static String getSiLAErrorType(SiLAFramework.SiLAError error) {
            if (error.ErrorCase == SiLAFramework.SiLAError.ErrorOneofCase.ExecutionError)
                return ("Execution");
            if (error.ErrorCase == SiLAFramework.SiLAError.ErrorOneofCase.FrameworkError)
                return ("Framework");
            if (error.ErrorCase == SiLAFramework.SiLAError.ErrorOneofCase.ReadError)
                return ("Read");
            if (error.ErrorCase == SiLAFramework.SiLAError.ErrorOneofCase.ValidationError)
                return ("Validation");
            return (null);
        }
    }
}