﻿namespace sila2.Discovery
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Tmds.MDns;

    public class ServiceFinder
    {
        private static ServiceBrowser _serviceBrowser;

        public class ServiceDescription
        {
            public string ServiceName;
            public string HostName;
            public string Address;
        }

        /// <summary>
        /// Queries for a service of the given type thats name contains the given pattern and returns the first matching occurence unless the timeout exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="serviceNamePattern">The pattern that has to be contained in the service name.</param>
        /// <param name="timeout">If the timeout excceeds before a matching service has been discovered, null is returned.</param>
        /// <returns>The first discovered service matching the given conditions or null if nothing has been discovered within the given timeout.</returns>
        public static ServiceDescription QueryAnyServiceByName(string serviceType, string serviceNamePattern, int timeout, bool exactMatch = false)
        {
            return QueryAnyService(serviceType, serviceNamePattern, string.Empty, timeout, exactMatch);
        }

        /// <summary>
        /// Queries for a service of the given type thats host name contains the given pattern and returns the first matching occurence unless the timeout exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="hostNamePattern">The pattern that has to be contained in the host name.</param>
        /// <param name="timeout">If the timeout excceeds before a matching service has been discovered, null is returned.</param>
        /// <returns>The first discovered service matching the given conditions or null if nothing has been discovered within the given timeout.</returns>
        public static ServiceDescription QueryAnyServiceByHostName(string serviceType, string hostNamePattern, int timeout, bool exactMatch = false)
        {
            return QueryAnyService(serviceType, string.Empty, hostNamePattern, timeout, exactMatch);
        }

        /// <summary>
        /// Queries for a service of the given type thats name and host name contain the given patterns and returns the first matching occurence unless the timeout exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="serviceNamePattern">The pattern that has to be contained in the service name.</param>
        /// <param name="hostNamePattern">The pattern that has to be contained in the host name.</param>
        /// <param name="timeout">If the timeout excceeds before a matching service has been discovered, null is returned.</param>
        /// <returns>The first discovered service matching the given conditions or null if nothing has been discovered within the given timeout.</returns>
        public static ServiceDescription QueryAnyService(string serviceType, string serviceNamePattern, string hostNamePattern, int timeout, bool exactMatch = false)
        {
            List<ServiceDescription> result;

            if (_serviceBrowser == null)
            {
                _serviceBrowser = new ServiceBrowser { QueryParameters = { QueryInterval = timeout / 10 } };
            }

            DateTime timeoutTime = DateTime.Now + new TimeSpan(0, 0, 0, 0, timeout);
            _serviceBrowser.StartBrowse(serviceType);

            do
            {
                Thread.Sleep(_serviceBrowser.QueryParameters.QueryInterval);
                result = CheckMatchingServices(serviceType, serviceNamePattern, hostNamePattern, exactMatch);
            } while (result.Count == 0 && timeoutTime > DateTime.Now);

            _serviceBrowser.StopBrowse();

            return result.Count > 0 ? result[0] : null;
        }

        /// <summary>
        /// Queries for a service of the given type thats name contains the given pattern and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="serviceNamePattern">The pattern that has to be contained in the services name.</param>
        /// <param name="time">If the time (in ms) that will be waited before the result is returned.</param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static List<ServiceDescription> QueryAllServicesByName(string serviceType, string serviceNamePattern, int time)
        {
            return QueryAllServices(serviceType, serviceNamePattern, string.Empty, time);
        }

        /// <summary>
        /// Queries for a service of the given type thats host name contains the given pattern and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="hostNamePattern">The pattern that has to be contained in the host name.</param>
        /// <param name="time">If the time (in ms) that will be waited before the result is returned.</param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static List<ServiceDescription> QueryAllServiceByHostName(string serviceType, string hostNamePattern, int time)
        {
            return QueryAllServices(serviceType, string.Empty, hostNamePattern, time);
        }

        /// <summary>
        /// Queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="serviceNamePattern">The pattern that has to be contained in the service name.</param>
        /// <param name="hostNamePattern">The pattern that has to be contained in the host name.</param>
        /// <param name="time">If the time (in ms) that will be waited before the result is returned.</param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static List<ServiceDescription> QueryAllServices(string serviceType, string serviceNamePattern, string hostNamePattern, int time)
        {
            if (_serviceBrowser == null)
            {
                _serviceBrowser = new ServiceBrowser { QueryParameters = { ResponseTime = time, QueryInterval = 0 } };
            }

            _serviceBrowser.StartBrowse(serviceType);
            Thread.Sleep(time);
            List<ServiceDescription> result = CheckMatchingServices(serviceType, serviceNamePattern, hostNamePattern);
            _serviceBrowser.StopBrowse();

            return result;
        }

        /// <summary>
        /// Checks the services discovered by the service browser for matching service names and host names.
        /// </summary>
        /// <param name="serviceType">The service type to be filtered for.</param>
        /// <param name="serviceNamePattern">The service name pattern to be filtered for.</param>
        /// <param name="hostNamePattern">The host name pattern to be filtered for.</param>
        /// <returns>The resulting list of services matching the conditinos.</returns>
        private static List<ServiceDescription> CheckMatchingServices(string serviceType, string serviceNamePattern, string hostNamePattern, bool exactMatch = false)
        {
            List<ServiceDescription> result = new List<ServiceDescription>();

            foreach (var service in _serviceBrowser.Services)
            {
                if (service.Type.Contains(serviceType)
                    && (string.IsNullOrEmpty(serviceNamePattern) || (exactMatch ? service.Instance == serviceNamePattern : service.Instance.Contains(serviceNamePattern)))
                    && (string.IsNullOrEmpty(hostNamePattern) || (exactMatch ? service.Hostname == hostNamePattern : service.Hostname.Contains(hostNamePattern))))
                {
                    result.Add(new ServiceDescription { ServiceName = service.Instance, HostName = service.Hostname, Address = service.Addresses[0] + ":" + service.Port });
                }
            }

            return result;
        }

        /// <summary>
        /// Asynchronously queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="serviceType">The service type to be querried for.</param>
        /// <param name="serviceNamePattern">The pattern that has to be contained in the service name.</param>
        /// <param name="hostNamePattern">The pattern that has to be contained in the host name.</param>
        /// <param name="time">If the time (in ms) that will be waited before the result is returned.</param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static async Task<List<ServiceDescription>> QueryAllServicesAsync(string serviceType, string serviceNamePattern, string hostNamePattern, int time)
        {
            if (_serviceBrowser == null)
            {
                _serviceBrowser = new ServiceBrowser { QueryParameters = { ResponseTime = time, QueryInterval = 0 } };
            }
            _serviceBrowser.StartBrowse(serviceType);
            await Task.Delay(time);
            List<ServiceDescription> result = CheckMatchingServices(serviceType, serviceNamePattern, hostNamePattern);
            _serviceBrowser.StopBrowse();

            return result;
        }

    }
}