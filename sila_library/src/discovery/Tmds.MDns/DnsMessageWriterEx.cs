﻿//Copyright (C) 2018  EQUIcon Software GmbH Jena

//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.

//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.

//You should have received a copy of the GNU Lesser General Public
//License along with this library; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Tmds.MDns
{
    /// <summary>
    /// Extensions for acting as service provider (Answering service discovery questions)
    /// Copyright by EQUIcon Software GmbH Jena
    /// </summary>
    partial class DnsMessageWriter
    {
        public void WriteAnswerHeader(Header queryHeader)
        {
            Debug.Assert(_stream.Position == 0);
            WriteUInt16(queryHeader.TransactionID);

            // Create the Flags (fixed because due test conditions) TODO: make it better
            ushort flags = 0;
            flags |= 1 << 15; // Is Answer
            flags |= 0; // 4 bit Opcode. for test set to 0
            flags |= 1 << 10; //
            // all other used 0 (including anay error code

            WriteUInt16(flags); // flags
            WriteUInt16(0); // questionCount
            WriteUInt16(0); // answerCount
            WriteUInt16(0); // authorityCount
            WriteUInt16(0); // additionalCount

        }

        public void WriteSrvRecord(RecordSection recordType, Name name, uint ttl, ushort priority, ushort weight, ushort port, Name target)
        {
            WriteRecordStart(recordType, name, RecordType.SRV, ttl, RecordClass.Internet);
            WriteUInt16(priority);
            WriteUInt16(weight);
            WriteUInt16(port);
            WriteName(target);
            WriteRecordEnd();
        }

        public void WriteTxtRecord(RecordSection recordType, Name name, uint ttl, List<string> txt)
        {
            WriteRecordStart(recordType, name, RecordType.TXT, ttl, RecordClass.Internet);
            if (txt == null || txt.Count == 0)
            {
                WriteRecordData(new byte[] { 0 });
            }
            else
            {
                foreach (string line in txt)
                {
                    WriteRecordData(new byte[] { (byte)line.Length });
                    WriteRecordData(Encoding.UTF8.GetBytes(line));
                }
            }
            WriteRecordEnd();
        }
    }
}
