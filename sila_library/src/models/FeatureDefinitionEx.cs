﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// This class extents the Feature class auto-generated from the FeatureDefinition schema.
/// </summary>
public partial class Feature
{
    /// <summary>
    /// Gets the fully qualified identifier assembled from several feature properties as specified in the SiLA 2 standard.
    /// </summary>
    public string FullyQualifiedIdentifier => $"{this.Originator}/{this.Category}/v{this.MajorVersion}/{this.Identifier}";

    /// <summary>
    /// Gets the namespace to be used when calling the gRPC service.
    /// </summary>
    public string Namespace => $"sila{SiLAVersion}.{Originator}.{Category}.{Identifier.ToLower()}.v{MajorVersion}";

    /// <summary>
    /// Returns all StandardExecutionErrors defined in the Feature definition.
    /// </summary>
    /// <returns>A list of all StandardExecutionErrors</returns>
    public List<FeatureStandardExecutionError> GetStandardExecutionErrors()
    {
        var result = new List<FeatureStandardExecutionError>();

        foreach (var item in Items)
        {
            if (item is FeatureStandardExecutionError error)
            {
                result.Add(error);
            }
        }

        return result;
    }

    #region Overrides of Object

    /// <summary>
    /// Lists the Featrue Definition content in a readable multiline manner.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();

        foreach (System.Reflection.PropertyInfo property in this.GetType().GetProperties())
        {
            switch (property.Name)
            {
                case "Items":
                    break;

                default:
                    sb.Append(property.Name);

                    sb.Append(": ");
                    if (property.GetIndexParameters().Length > 0)
                    {
                        sb.Append("Indexed Property cannot be used");
                    }
                    else
                    {
                        sb.Append(property.GetValue(this, null));
                    }

                    sb.Append(Environment.NewLine);
                    break;
            }
        }

        // items
        sb.Append(Environment.NewLine);
        foreach (var item in this.Items)
        {
            if (item is FeatureCommand command)
            {
                sb.AppendFormat("<Command> \"{0}\": {1}\n", command.Identifier, command.Description);
                if (command.Parameter != null)
                {
                    sb.AppendLine("   Parameters:");
                    foreach (var parameter in command.Parameter)
                    {
                        sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", parameter.Identifier, GetDataTypeString(parameter.DataType), parameter.Description);
                    }
                }

                if (command.Response != null)
                {
                    sb.AppendLine("   Responses:");
                    foreach (var response in command.Response)
                    {
                        sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", response.Identifier, GetDataTypeString(response.DataType), response.Description);
                    }
                }

                if (command.IntermediateResponse != null)
                {
                    sb.AppendLine("   Intermediate Responses:");
                    foreach (var intermediateResponse in command.IntermediateResponse)
                    {
                        sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", intermediateResponse.Identifier, GetDataTypeString(intermediateResponse.DataType), intermediateResponse.Description);
                    }
                }

                if (command.StandardExecutionErrors != null)
                {
                    sb.AppendLine("   Standard Execution Errors:");
                    foreach (var error in command.StandardExecutionErrors)
                    {
                        sb.AppendFormat("   - \"{0}\"\n", error);
                    }
                }

            }
            else if (item is FeatureStandardExecutionError error)
            {
                sb.AppendFormat("<StandardExecutionError> \"{0}\": {1}\n", error.Identifier, error.Description);
            }
            else if (item is FeatureProperty property)
            {
                sb.AppendFormat("<Property> \"{0}\" {1} [{2}]: {3}\n", property.Identifier, property.Observable, GetDataTypeString(property.DataType), property.Description);
            }
            else if (item is FeatureMetadata metadata)
            {
                sb.AppendFormat("<Metadata> \"{0}\" [{1}]: {2}\n", metadata.Identifier, GetDataTypeString(metadata.DataType), metadata.Description);
            }
            else if (item is FeatureStandardReadError readError)
            {
                sb.AppendFormat("<StandardReadError> \"{0}\": {1}\n", readError.Identifier, readError.Description);
            }
            else if (item is FeatureDataTypeDefinition dataType)
            {
                sb.AppendFormat("<DataTypeDefinition> \"{0}\" [{1}]: {2}\n", dataType.Identifier, GetDataTypeString(dataType.DataType), dataType.Description);
            }
        }

        return sb.ToString();
    }

    private static string GetDataTypeString(DataTypeType type)
    {
        if (type.Item is BasicType)
        {
            // basic type
            return type.Item.ToString();
        }

        if (type.Item is ConstrainedType)
        {
            // contrained type
            return string.Format("constrained {0}", GetDataTypeString(((ConstrainedType)type.Item).DataType));
        }

        if (type.Item is StructureType structureType)
        {
            // structure type
            return string.Format("structured {0}", structureType.Element);
        }

        if (type.Item is ListType listType)
        {
            // list type
            return string.Format("list of {0}", GetDataTypeString(listType.DataType));
        }

        // custom type
        return type.Item.ToString();
    }

    #endregion
}
