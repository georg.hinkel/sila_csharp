**Important**: This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point. It might not comply with the latest version of the Standard, and much of its content might change in the future.

The first release candidate of SiLA 2 is scheduled for end of August 2018.

For more general information about the standard, we refer to the [sila_base](https://gitlab.com/SiLA2/sila_base) repository.

# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)

# SiLA C#

This repository contains examples, libraries and reference implementations of the SiLA 2 standard using the language C#.

It is intended to provide the content for all operating systems that support .NET Core platform (Microsoft Windows, 
Linux, macOS). By now the project infrastructure is only provided for Windows Visual Studio but can also be build 
via the .NET CLI, the C# code itself as well as the protobuf compiler scripts can be used on all platforms.

**Note**: This repository uses [sila_base](https://gitlab.com/SiLA2/sila_base) as a submodule. When cloning 
the repository from git make sure that the option `recurse-submodules` is used to get also the content of 
the submnodule.

	`$ git clone --recurse-submodules https://gitlab.com/SiLA2/sila_csharp`

As an entry point, refer to `sila_implementations/FirstExample`.

## Maintainer
This repository is maintained by:

 * Stefan Koch ([koch@equicon.de](mailto:koch@equicon.de)) of [EQUIcon Software GmbH](http://www.equicon.de/en/) 
 * Ricardo Gaviria ([ricardo@unitelabs.ch](mailto:ricardo@unitelabs.ch)) of [UniteLabs](http://www.unitelabs.ch)

## Components
### sila_implementations
Device drivers conforming with the SiLA standard.

### sila_library
The SiLA Library provides base classes for implementing SiLA Devices (and SiLA Clients in the future) according to Part A+B of the SiLA 2 Specification.

### sila_tools
Tools such as the protobuf compilers of gRPC (will contain also code generators in the furture).

## Installation / Pre-requisites
The code has been tested with Microsoft Visual Studio 2017 Version 15.5.6 and .net Core Framework 2.0. 

**Warning**: Do not use .net Core v2.1 on Linux, there is a known problem with port reusability that
cause problem with the mDNS discovery. 
Please use version 2.0 for now and make sure that the following command `dotnet --version` return `2.0.0`.
This problem might be fixed in the next version (2.2).

To build the sample applications under Microsoft Windows Visual Studio 2017 (Version 15.3 or higher) is required.
There are also free editions of Visual Studio like Express or Community Edition.

Install [.net Core SDK] (https://www.microsoft.com/net/download/Windows/build)

Install [gRPC C#] (https://github.com/grpc/grpc/tree/master/src/csharp)

## Running the Examples

### Getting started on Microsoft Windows
Just open the `sila_implementations/sila_implementations.sln` and have a lookt at the `FirstExample` folder.
The ExampleServer project contains all classes of a SiLA 2 device that implements two example Features defined in the sila_base repository: a GreetingProvider and a TemperatureController feature.
The ExampleClient project builds a SiLA 2 client using the functionality provided by the example device.
Both projects refer to the SiLA2Library project, containing base classes, needed to implement SiLA 2 devices or clients.

### Getting Started using the .NET CLI
The sample applications can just be run from the command line using the `dotnet` cli.
Taking the ExampleServer as an example lets run the c# server and client.

```bash
cd sila_implementations/FirstExample/ExampleServer/
dotnet build
dotnet run
```

Now its time to run the Client application, on a separate terminal execute the following set of commands
```bash
cd sila_implementations/FirstExample/ExampleClient/
dotnet build
dotnet run
```

## Developing a Driver which requires .NET Framework support
`sila_csharp` is built using .NET standard which is the base layer libraries which can be used by both the .NET core 
and .NET framework runtimes. Therefore unless otherwise necessary your base libraries should be built for .NET standard
such that .NET core and .NET framework applications can use them.

In most cases, one might want to use `sila_csharp` to be able to interact with device API's which offer 
a .NET framework API via some .dll. In this case your Device driver application should be built against 
a .NET Framwork .

In most cases, one might want to use the `sila_csharp/SiLA2Library` in the implementation that connects 
the SiLA commands with the device API (e.g. a .NET API from a DLL).

To support compilation of this adapter against both .NET Core and .NET Framework, we recommend that you 
implement it as a .NET Standard class library.

 [TODO: Example to be given here]


## Compiling proto files

To compile the protobuf files into C# stubs used by the implementations, the proto compiler(s) of the Grpc.Tools package are used.
Depending on your operating system execute the script

### On Windows

```bash
generate_stubs.cmd
```

### On Linux/MacOS
```bash
generate_stubs.sh
```

Note: As the required C# stubs are contained in the repository you don't need to compile the protobuf files to be able to build 
the examples. Only modifications of the protobuf files will require a new compilation.